import pandas as pd
import utils
import FPLlib
# import sys
TEAMS = FPLlib.TEAM_INDICES
# sys.stdout=open("threeway2019_.txt","w")
(GW_START, GW_END) = (1,7)
MIN_DIFFICULTY = 3
expensive_defs = FPLlib.EXPENSIVE_DEFENDERS

def gameweek_filter(df):
  df = df.query( f"event >={GW_START} & event <={GW_END} & team_h_difficulty <={MIN_DIFFICULTY}& team_h != {expensive_defs}")
  return df


df = pd.DataFrame(FPLlib.getFPLData())
filtered_data = gameweek_filter(df)
easy_gws =  pd.DataFrame(filtered_data).groupby('team_h')['event'].apply(list)
three_way_rotation = utils.rotation(easy_gws.index.values.tolist(), 2)
for i in three_way_rotation:
  (a,b) = i
  
  hard_fix = list(set([i for i in range(GW_START,GW_END+1)]) - set().union(easy_gws[a]+easy_gws[b]))
  if len(hard_fix) == 0:
    print(f"Teams: ({TEAMS.get(str(a))}, {TEAMS.get(str(b))})")
    print(f"{TEAMS.get(str(a))} - {easy_gws[a]}")
    print(f"{TEAMS.get(str(b))}  - {easy_gws[b]}")
    print("----------------------------------------")
# sys.stdout.close()
