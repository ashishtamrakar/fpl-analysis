import pandas as pd
import utils
import FPLlib
# import sys
TEAMS = FPLlib.TEAM_INDICES
# sys.stdout=open("threeway2019_.txt","w")
(GW_START, GW_END) = (1,19)
MIN_DIFFICULTY = 3
def gameweek_filter(df, home_away):
  df = df.query( f"event >={GW_START} & event <={GW_END} & {home_away}_difficulty <= {MIN_DIFFICULTY} ")
  return df

def getGameweeks(fix, team):
  returnData =[];
  for i in team:
    returnData += fix[str(i)]
  return returnData  

def getData(data, team):
  if team in data:
    return data[team]
  else:
    return []


df = pd.DataFrame(FPLlib.getFPLData())
filtered_data_home = gameweek_filter(df,'team_h')
filtered_data_away = gameweek_filter(df,'team_a')
fix = {}
easy_gws_home =  pd.DataFrame(filtered_data_home).groupby('team_h')['event'].apply(list)
easy_gws_away =  pd.DataFrame(filtered_data_away).groupby('team_a')['event'].apply(list)
for i in FPLlib.TEAM_INDICES:
  a = getData(easy_gws_away,int(i)) + getData(easy_gws_home,int(i))
  fix[i] = a
three_way_rotation = utils.rotation(range(1,21), 5)
for i in three_way_rotation:
  (a,b,c,d,e) = i
  total = 0
  for j in range(GW_START, GW_END+1):
    count = 0
    for team in i:
      fixtures = fix[str(team)]
      if j in fixtures:
        count +=1
    if count >= 3:
      total += 1
  if total == GW_END-GW_START+1:
    print(i,total)
  # if len(easy_fix) == GW_END - GW_START + 1:
    # print(i,easy_fix)
  # total = 0
  # print(i)
  # for j in range(GW_START, GW_END+1):
  #   count = 0
  #   for k in i:
  #     if j in easy_gws[k]:
  #       count += 1
  #   if count >= 3:
  #     total += count;
  # print(total) 
  # if total >= 3*(GW_END - GW_START+1):
  #   print(i,j,total)    
#   hard_fix = list(set([i for i in range(GW_START,GW_END+1)]) - set().union(easy_gws[a]+easy_gws[b] + easy_gws[c]))
#   if len(hard_fix) == 0:
#     print(f"Teams: ({TEAMS.get(str(a))}, {TEAMS.get(str(b))}, {TEAMS.get(str(c))})")
#     print(f"{TEAMS.get(str(a))} - {easy_gws[a]}")
#     print(f"{TEAMS.get(str(b))}  - {easy_gws[b]}")
#     print(f"{TEAMS.get(str(c))}  - {easy_gws[c]}")   
#     print("----------------------------------------")
# # sys.stdout.close()
