
import pandas as pd
import utils
# import sys
import FPLlib
# import sys
TEAMS = FPLlib.TEAM_INDICES
# sys.stdout=open("threeway2019_.txt","w")
(GW_START, GW_END) = (1,9)
MIN_DIFFICULTY = 2
def gameweek_filter(df, home_away):
  df = df.query( f"event >={GW_START} & event <={GW_END} & {home_away}_difficulty <= {MIN_DIFFICULTY}")
  return df

def getGameweeks(data, team):
  if team in data:
    return data[team]
  else:
    return []

df = pd.DataFrame(FPLlib.getFPLData())
filtered_data_home = gameweek_filter(df,'team_h')
filtered_data_away = gameweek_filter(df,'team_a')
easy_gws_home =  pd.DataFrame(filtered_data_home).groupby('team_h')['event'].apply(list)
easy_gws_away =  pd.DataFrame(filtered_data_away).groupby('team_a')['event'].apply(list)
easy_gws = list(set().union(list(easy_gws_away.index.values)+ list(easy_gws_home.index.values)))
print(easy_gws)
two_way_rotation = utils.rotation(easy_gws,2)
outputData = {}
for i in two_way_rotation:
  (a,b) = i
  
  hard_fix = list(set([i for i in range(GW_START,GW_END+1)]) - set().union(getGameweeks(easy_gws_home, a) +  getGameweeks(easy_gws_away, a)+getGameweeks(easy_gws_home, b) +  getGameweeks(easy_gws_away, b)))
  
  outputData[f"Teams: ({TEAMS.get(str(a))}, {TEAMS.get(str(b))})"] = len(hard_fix)
  if len(hard_fix) == 0:
    print(f"Teams: ({TEAMS.get(str(a))}, {TEAMS.get(str(b))})")
    if (a,b) in easy_gws_home:
      print(f"Home: {a} - {easy_gws_home[a]}")
      print(f"Home: {b} - {easy_gws_home[b]}")
    if (a,b) in easy_gws_away:
      print(f"Away: {a} - {easy_gws_away[a]}")   
      print(f"Away: {b} - {easy_gws_away[b]}")   

# print(sorted(outputData.items(), key=lambda kv: kv[1]))
# sys.stdout.close()
