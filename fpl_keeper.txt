import json
import requests
import pandas as pd
from itertools import combinations
# import sys

# sys.stdout=open("threeway2019_.txt","w")



def gameweek_filter(df, gw_start, gw_end):
  expensive_defs = [1, 6, 8,10,11,12,17]
  df = df.query( f"event >={gw_start} & event <={gw_end} & team_h_difficulty <=3 & team_h != {expensive_defs}")
  return df


response = requests.get("https://fantasy.premierleague.com/api/fixtures/")
data = json.loads(response.text)
df = pd.DataFrame(data)
filtered_data = gameweek_filter(df, 1, 17)
easy_gws =  pd.DataFrame(filtered_data).groupby('team_h')['event'].apply(list)
three_way_rotation = combinations(easy_gws.index.values.tolist(),3)
for i in three_way_rotation:
  (a,b,c) = i
  
  hard_fix = list(set([i for i in range(1,18)]) - set().union(easy_gws[a]+easy_gws[b] + easy_gws[c]))
  if len(hard_fix) == 0:
    print(f"Teams: {i}")
    print(f"{a} - {easy_gws[a]}")
    print(f"{b} - {easy_gws[b]}")
    print(f"{c} - {easy_gws[c]}")   
    print("----------------------------------------")
# sys.stdout.close()
