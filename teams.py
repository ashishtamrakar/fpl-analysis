import json
import requests
teamIndices = {
  "1": "Arsenal",
  "2":"Aston Villa",
  "3":"Bournemouth",
  "4":"Brighton",
  "5":"Burnley",
  "6":"Chelsea",
  "7":"Crystal Palace",
  "8":"Everton",
  "9":"Leicester",
  "10":"Liverpool",
  "11":"Manchester City",
  "12":"Manchester United",
  "13":"Newcastle",
  "14":"Norwich",
  "15":"Sheffield United",
  "16":"Southampton",
  "17":"Tottenham",
  "18":"Watform",
  "19":"West Ham",
  "20":"Wolves"
}

EXPENSIVE_DEFENDERS = [1, 6, 8,10,11,12,17]
EXPENSIVE_GKS = [1, 6,7, 8, 9,10,11,12,13,17,18,19,20]

API_URL = "https://fantasy.premierleague.com/api/fixtures/"
def getFPLData():
  response = requests.get(API_URL)
  data = json.loads(response.text)
  return data