
import pandas as pd
import utils
import numpy as np
# import sys
import FPLlib
# import sys
TEAMS = FPLlib.TEAM_INDICES
# sys.stdout=open("threeway2019_.txt","w")
(GW_START, GW_END) = (6,6)

MIN_DIFFICULTY = 2

def gameweek_filter(df, home_away):
  df = df.query( f"event >={GW_START} & event <={GW_END}")
  return df

def getGameweeks(data, team):
  if team in data:
    return data[team]
  else:
    return []

df = pd.DataFrame(FPLlib.getFPLData())
filtered_data_home = gameweek_filter(df,'team_h')
filtered_data_away = gameweek_filter(df,'team_a')
# easy_gws_home =  pd.DataFrame(filtered_data_home).groupby('event')['team_h'].apply(list)
# easy_gws_away =  pd.DataFrame(filtered_data_away).groupby('event')['team_a'].apply(list)

easy_gws_home =  pd.DataFrame(filtered_data_home).groupby('team_h')
easy_gws_away =  pd.DataFrame(filtered_data_away).groupby('team_a')

away_diff = easy_gws_away['team_a_difficulty'].agg([np.sum])
home_diff = easy_gws_home['team_h_difficulty'].agg([np.sum])

df_add = home_diff.add(away_diff, fill_value=0)
# print(home_diff.loc[home_diff['mean']<=3])
# print(away_diff.loc[away_diff['mean']<=3])
print(df_add)

# for i in FPLlib.ATTACKING_TEAMS:
#   all_fix = getGameweeks(easy_gws_away,i)+getGameweeks(easy_gws_home,i)
#   # print(all_fix)
#   if len(all_fix) == GW_END - GW_START + 1:
#     print(i)