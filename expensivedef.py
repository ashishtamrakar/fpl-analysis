
import pandas as pd
import utils
# import sys
import FPLlib
# import sys
TEAMS = FPLlib.TEAM_INDICES
# sys.stdout=open("threeway2019_.txt","w")
(GW_START, GW_END) = (1,15)
MIN_DIFFICULTY = 2
expensive_defs = FPLlib.DEF_DEFENDERS
def gameweek_filter(df, home_away):
  df = df.query( f"event >={GW_START} & event <={GW_END} & {home_away}_difficulty <= {MIN_DIFFICULTY} &  {home_away}== {expensive_defs}")
  return df

def getGameweeks(data, team):
  if team in data:
    return data[team]
  else:
    return []

df = pd.DataFrame(FPLlib.getFPLData())
filtered_data_home = gameweek_filter(df,'team_h')
filtered_data_away = gameweek_filter(df,'team_a')
easy_gws_home =  pd.DataFrame(filtered_data_home).groupby('team_h')['event'].apply(list)
easy_gws_away =  pd.DataFrame(filtered_data_away).groupby('team_a')['event'].apply(list)
easy_gws = list(set().union(list(easy_gws_away.index.values)+ list(easy_gws_home.index.values)))

two_way_rotation = utils.rotation(easy_gws,3)
outputData = {}
for i in two_way_rotation:
  (a,b,c) = i
  hard_fix1 = list(getGameweeks(easy_gws_home, a) +  getGameweeks(easy_gws_away, a))
  hard_fix2 = list(getGameweeks(easy_gws_home, b) +  getGameweeks(easy_gws_away, b))
  hard_fix3 = list(getGameweeks(easy_gws_home, c) +  getGameweeks(easy_gws_away, c))
  
  total = len(hard_fix1)+len(hard_fix2) + len(hard_fix3)
  # print(f"({TEAMS.get(str(a))}, {TEAMS.get(str(b))})")
  # fixtures = [*hard_fix1, *hard_fix2]
  # print(sorted(fixtures))
  outputData[f"({TEAMS.get(str(a))}, {TEAMS.get(str(b))}, {TEAMS.get(str(c))})"] = total
print(sorted(outputData.items(), key=lambda kv: kv[1],reverse = True))

# '(Chelsea, Liverpool)', 33), ('(Everton, Liverpool)', 33), ('(Leicester, Liverpool)', 33), ('(Liverpool, Manchester City)', 33), ('(Liverpool, Manchester United)', 33)
# '(Chelsea, Everton)', 25), ('(Chelsea, Leicester)', 25), ('(Chelsea, Liverpool)', 25), ('(Chelsea, Manchester United)', 25), ('(Chelsea, Tottenham)', 25),
# '(Chelsea, Leicester)', 19), ('(Leicester, Liverpool)', 19),('(Chelsea, Liverpool)', 18), ('(Everton, Leicester)', 18),